let _elements = {};
let _status = {};
let _slots  = [];

const _levelConfig = {
	EASY   : { time: 1000, points: 10 },
	NORMAL : { time: 750,  points: 20 },
	HARD   : { time: 500,  points: 30 },
};

document.addEventListener('DOMContentLoaded', () => {
	_elements = collectElements();
	_status = createStatus();

	if (!_status.userName) {
		window.location.href = 'home.html';
		return;
	}

	_slots = createSlots();

	createPlayground();
	stop();
});

function collectElements() {
	return {
		userName     : document.querySelector('#header-user__name'),
		levelSelect  : document.querySelector('#level-select'),
		score        : document.querySelector('#score__value'),
		playground   : document.querySelector('#playground'),
		startStopBtn : document.querySelector('#start-stop-btn'),
	}
}

function createStatus() {
	const status = {
		selectedLevel: _levelConfig[_elements.levelSelect.value],
		timer: null,

		get userName() {
			return this._userName;
		},

		set userName(value) {
			if (value) {
				this._userName = value;
			
			} else {
				const urlParams = new URLSearchParams(window.location.search);
				this._userName = urlParams.get('username');
			}
			
			const userNameText = document.createTextNode(this._userName || '');
			_elements.userName.appendChild(userNameText);
		},

		get score() {
			return this._score || 0;
		},

		set score(value) {
			this._score = value;
			updateScore();
		},

		set activeSlotIndex(slotIndex) {
			this._activeSlot = null;

			_slots.forEach((each, index) => {
				if (slotIndex === index) {
					each.setAttribute('is-active', '');
					this._activeSlot = each;
				
				} else {
					each.removeAttribute('is-active');
				}
			});
		}
	};

	status.userName = null;
	return status;
}

function createSlots() {

	return [
		'upper-left' , 'upper-center' , 'upper-right' ,
		'middle-left', 'middle-center', 'middle-right',
		'bottom-left', 'bottom-center', 'bottom-right',
	
	].map(eachZone => {
		const slot = document.createElement('mole-slot');
		slot.setAttribute('id', eachZone);

		slot.addEventListener('mole-hit', event => {
			moleHit(event.detail);
		});

		slot.addEventListener('mole-miss', event => {
			moleMiss(event.detail);
		});

		return slot;
	});
}

function createPlayground() {
	_slots.forEach(eachSlot => {
		_elements.playground.appendChild(eachSlot);
	});
}

function start() {

	while (_elements.startStopBtn.hasChildNodes())
	_elements.startStopBtn.removeChild(_elements.startStopBtn.firstChild);
	
	_elements.startStopBtn.removeEventListener('click', start);

	_elements.startStopBtn.appendChild(document.createTextNode('Stop'));
	_elements.startStopBtn.addEventListener('click', stop);

	_elements.levelSelect.setAttribute('disabled', '');

	_status.selectedLevel = _levelConfig[_elements.levelSelect.value];

	_slots.forEach(eachSlot => eachSlot.setAttribute('enabled', ''));

	_status.timer = setInterval(() => {
		const index = Math.floor((Math.random() / 1) * 9);
		_status.activeSlotIndex = index;
	}, _status.selectedLevel.time);
}

function stop() {
	clearInterval(_status.timer);
	_status.activeSlotIndex = null;
	_status.score = 0;

	while (_elements.startStopBtn.hasChildNodes())
	_elements.startStopBtn.removeChild(_elements.startStopBtn.firstChild);
	
	_elements.startStopBtn.removeEventListener('click', stop);

	_elements.startStopBtn.appendChild(document.createTextNode('Start'));
	_elements.startStopBtn.addEventListener('click', start);

	_elements.levelSelect.removeAttribute('disabled');

	_slots.forEach(eachSlot => eachSlot.removeAttribute('enabled'));
}

function moleHit(mole) {
	mole.removeAttribute('is-active');
	_status.score += _status.selectedLevel.points;

	const floatingFeedback = createFloatingFeedback(mole);
	_elements.playground.appendChild(floatingFeedback);

	setTimeout(() => {
		_elements.playground.removeChild(floatingFeedback);
	}, 500);
}

function moleMiss(mole) {
	_status.score -= calculateLostPoints();

	const floatingFeedback = createFloatingFeedback(mole, 'negative');
	_elements.playground.appendChild(floatingFeedback);

	setTimeout(() => {
		_elements.playground.removeChild(floatingFeedback);
	}, 500);
}

function calculateLostPoints() {
	return Math.floor(_status.selectedLevel.points / 3);
}

function createFloatingFeedback(mole, variant = 'positive') {
	const el = document.createElement('span');
	el.setAttribute('class', `floating-feedback floating-feedback--${variant}`);

	const isPositive = variant === 'positive';
	const sign = isPositive ? '+' : '-';
	const quantity = isPositive ? _status.selectedLevel.points : calculateLostPoints();
	const floatingText = `${sign}${quantity}`;

	const extraLeftOffset = floatingText.length / 2 - 0.5;

	el.style.left = `calc(${mole.offsetLeft + (mole.offsetWidth / 2)}px - ${extraLeftOffset}em)`;
	el.style.top = `${mole.offsetTop + (mole.offsetHeight / 2)}px`;

	const textNode = document.createTextNode(floatingText);
	el.appendChild(textNode);

	return el;
}

function updateScore() {
	while (_elements.score.hasChildNodes())
		_elements.score.removeChild(_elements.score.firstChild);
	
	_elements.score.appendChild(document.createTextNode(_status.score));
}