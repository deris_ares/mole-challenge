customElements.define('mole-slot', class extends HTMLElement {
	
	connectedCallback() {
		this.addEventListener('click', () => {

			if (!this.hasAttribute('enabled')) {
				return;
			}
		
			if (this.hasAttribute('is-active')) {
				this.dispatchEvent(new CustomEvent('mole-hit', { detail: this }));
			
			} else {
				this.dispatchEvent(new CustomEvent('mole-miss', { detail: this }));
			}
		});
	}
});
