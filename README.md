# Mole Challenge

## What is this all about?

Mole Challenge is a web based, mobile targeted quick and dirty Wackamole game. Do not expect 8K Raytracing capabilities 😉

## How to use
This is a barebones, endless, demonstration-purposed version of Whack-A-Mole game

The objective is to "whack" as many moles as possible. In the first screen you will be prompted for your username, so please enter it and press Join. You will get to the game scene. There is a Start button at the bottom of the screen. If you press it, well... it starts 😆

But wait! At the top right corner you can see a dropdown containing three levels of difficulty. They condition the rate at which the moles appears and dissapears, but also the points scored for each hit. Select one (or leave the default) and you are done. Whack a mole!

PS: Be careful! This evil version makes you loss score points if you miss a hit 😈

PPS: You can stop the game with the same Start button, which now will show the Stop text

## License
You can do virtually whatever you want with this project, as long as your derived projects are distributed under the same GPLv3 license. More info in [LICENSE.txt](https://gitlab.com/deris_ares/mole-challenge/-/blob/main/LICENSE.txt) file

## Online Demo
You can test it out here: [https://deris_ares.gitlab.io/mole-challenge/view/home.html](https://deris_ares.gitlab.io/mole-challenge/view/home.html)

## Android native APK
You can also download and install a pre-built, self-contained native Android APK from [the build folder](https://gitlab.com/deris_ares/mole-challenge/-/tree/main/build). More possibilities = good 👍

## Devs section

### Clone
`git clone https://gitlab.com/deris_ares/mole-challenge.git`

### Install
`npm install`

### Run
#### In web browser with live reload capabilities
`npm run serve`

#### In Android physical device (*)
`npm run android`

#### In Android emulated device (*)
`npm run emulate`

(*) Android setup must be performed first in order to run or compile. See the next "Android" subsection under "Build" category

### In iOS device
I don't own an iOS device so native iOS compatibility is not guaranteed

### Build
The project has been developed under Linux (Ubuntu) environment, so the following instructions have been tested on it

#### Android
You will need the Android SDK to be able to build for this platform. The first step is installing it. In Debian-derived Linux distributions, it is possible by executing `sudo apt update && sudo apt install android-sdk`

Also JDK is required. This can be installed by running `sudo apt install openjdk-11-jdk`

Install Android SDK Manager required packages
```
sudo apt install sdkmanager
sudo sdkmanager "build-tools;33.0.2"
sudo sdkmanager --licenses (select 'y' when prompted)
sudo cp -r /opt/android-sdk/build-tools/33.0.2/ /usr/lib/android-sdk/build-tools/
sudo cp /opt/android-sdk/licenses/* /usr/lib/android-sdk/licenses/
```

You can build the APK by running `npm run build`

Now, the application can be run in a physically connected device by running `npm run android`