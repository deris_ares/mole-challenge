const filesToBeCached = [
	'./view/home.html',
	'./view/game.html',

	'./js/home.js',
	'./js/game.js',
	'./js/component/mole-slot.js',

	'./css/common.css',
	'./css/home.css',
	'./css/game.css',

	'./img/mole.jpg'
];

self.addEventListener('install', event => {
	event.waitUntil(
		caches.open('mole-challenge').then(cache =>
			cache.addAll(filesToBeCached)
		)
	);
});

self.addEventListener('fetch', event => {
	event.respondWith(
		caches.match(event.request, { ignoreSearch: true }).then(response =>
			response || fetch(event.request)
		)
	);
});
