document.addEventListener('DOMContentLoaded', () => {
	const joinButton = document.querySelector('#join-button');
	joinButton.addEventListener('click', join);
});

function join() {
	const userName = document.querySelector('#user-name').value;

	if (!userName) {
		alert('You must enter a user name');
		return;
	}

	document.location.href = `game.html?username=${userName}`;
}